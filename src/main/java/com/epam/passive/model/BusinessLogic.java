package com.epam.passive.model;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.*;

public class BusinessLogic implements Model {

    private static Logger logger1 = LogManager.getLogger(BusinessLogic.class);

    public BusinessLogic() {
    }

    @MyCustomAnotation(name="Lamda")
    public void maxWithLamda(int fa, int fb, int fc) {
        IFunctional lamda = (a, b, c) -> a > b ? Math.max(a, c) : Math.max(b, c);
        logger1.info("Max is " + lamda.fun(fa, fb, fc));
    }
    @MyCustomAnotation
    public String patternAsLamda(String message) {
        IcommandLamda lamda = x -> x;
        return lamda.command(message);
    }
    @MyCustomAnotation(name="Anonimus")
    public String patternAsAnonimus(String message) {
        IcommandLamda anonim = new IcommandLamda() {
            @Override
            public String command(String message) {
                return message;
            }
        };
        return anonim.command(message);
    }

    public void averageWithLamda(int fa, int fb, int fc) {
        IFunctional lamda = (a, b, c) -> (a + b + c) / 3;
        logger1.info("Average is " + lamda.fun(fa, fb, fc));
    }

    public List<Integer> generateWithStreamOf() {
        Stream<Integer> stream = Stream.of(5, 3, 5, 2, 6, 2);
        List<Integer> list = stream.collect(Collectors.toList());
        return list;
    }

    public List<Integer> generateWithIterated() {
        Stream<Integer> stream = Stream.iterate(25, n -> n - 2).limit(10);
        List<Integer> list = stream.collect(Collectors.toList());
        return list;
    }

    public List<Integer> generateWithStreamGenerated() {
        Stream<Integer> stream = Stream.generate(() -> 5);
        List<Integer> list = stream.collect(Collectors.toList());
        return list;
    }

    public void forthTask(String message) {

    }


}
