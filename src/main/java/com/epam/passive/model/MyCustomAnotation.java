package com.epam.passive.model;

public @interface MyCustomAnotation {
    String name() default "Moris";
}
