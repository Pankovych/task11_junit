package com.epam.passive.model;

import java.util.List;

public interface Model {

    void maxWithLamda(int fa, int fb, int fc);
    void averageWithLamda(int fa, int fb, int fc);
    List<Integer> generateWithStreamOf();
    List<Integer> generateWithIterated();
    List<Integer> generateWithStreamGenerated();
    String patternAsLamda(String message);
    String patternAsAnonimus(String message);

}
